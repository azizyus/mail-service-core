<form action="{{route("mailService.exampleGateway")}}" method="POST">
    {{csrf_field()}}
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="form-group">
                <div class="city">
                    <input type="hidden" name="{{\Azizyus\MailService\Enums\RequestEnums::_emailKey}}" value="contact_email">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <label for="city_1">Istanbul</label>
                            <input name="data[label:City][]" value="Istanbul" type="checkbox" class="form-control" id="city_1">
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <label for="city_2">Antalya</label>
                            <input name="data[label:City][]" value="Antalya" type="checkbox" class="form-control" id="city_2">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="city_3">Bursa</label>
                            <input name="data[label:City][]" value="Bursa" type="checkbox" class="form-control" id="city_3">
                        </div>
                        <div class="col-md-6">
                            <label for="city_4">Cappadocia</label>
                            <input name="data[label:City][]" value="Cappadocia" type="checkbox" class="form-control" id="city_4">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="city_5">Ephesus</label>
                            <input name="data[label:City][]" value="Ephesus" type="checkbox" class="form-control" id="city_5">
                        </div>
                        <div class="col-md-6">
                            <label for="city_6">Gallipoli</label>
                            <input name="data[label:City][]" value="Gallipoli" type="checkbox" class="form-control" id="city_6">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="city_7">Troy</label>
                            <input name="data[label:City][]" value="Troy" type="checkbox" class="form-control" id="city_7">
                        </div>
                        <div class="col-md-6">
                            <label for="city_8">Pergamon</label>
                            <input name="data[label:City][]" value="Pergamon" type="checkbox" class="form-control" id="city_8">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="city_9">Pamukkale</label>
                            <input name="data[label:City][]" checked="checked" value="Pamukkale" type="checkbox" class="form-control" id="city_9">
                        </div>
                        <div class="col-md-6">
                            <label for="city_10">Konya</label>
                            <input name="data[label:City][]" checked="checked" value="Konya" type="checkbox" class="form-control" id="city_10">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-2">
            <div class="contact-info">
                <input value="can" name="data[label:Name Surname]" type="text" required class="form-control" placeholder="Name Surname">
                <input value="can@diffea.com" name="data[label:Email]" type="text" required class="form-control" placeholder="Your Email address">
                <input value="can@diffea.com" type="text" required="required" class="form-control" placeholder="Confirm Your Email">
                <input value="phone-number" name="data[label:Phone]" type="text" required class="form-control" placeholder="Your Phone Number">
            </div>
        </div>
        <div class="col-md-2">
            <div class="contact-date">
                <input value="5" type="text" required class="form-control" placeholder="How Many Days?">
                <p>Desire Tour Date</p>
                <div class="float-left w-100">
                    <select name="data[label:Date,pair:Dates,sort:1]" id="" class="form-control  w-auto">
                        @for($i=1;$i<31;$i++)
                            <option value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>
                    <select name="data[label:Date,pair:Dates,sort:2]" id="" class=" form-control w-auto">
                        @for($i=1; $i<=12 ; $i++)
                            <option value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>
                    <select name="data[label:Date,pair:Dates,sort:3]" id="" class=" form-control w-auto">
                        @foreach([date("Y",time()),date('Y',strtotime(date("Y", time()) . " + 365 day"))] as $item)
                            <option value="{{$item}}">{{$item}}</option>
                        @endforeach
                    </select>
                </div>
                <p>Select Hotel Type</p>
                <div class="hotel-type">
                    <select name="data[label:Hotel]" id="hotel" class=" form-control w-100">
                        <option value="Suit">Suit</option>
                    </select>
                </div>

            </div>
        </div>
        <div class="col-md-2">
            <div class="contact-country">
                <input value="your-country" name="data[label:Your Country]" type="text" placeholder="Your Country" required class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <div class="message">
                <textarea name="data[label:Message]" id="message"  rows="3" placeholder="Message"></textarea>
                <button class="btn">REQUEST</button>
            </div>
        </div>
    </div>
</form>