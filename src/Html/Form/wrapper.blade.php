<div class="col-md-12">
    <h3>{{$header}}</h3>

    {!! $form !!}

    <div class="row">
      <div class="col-md-12">
          <butotn type="button" data-email-key="{{$emailKey}}"  class="btn btn-danger btn-sm remove-btn">Remove</butotn>
      </div>
    </div>
</div>
<hr>