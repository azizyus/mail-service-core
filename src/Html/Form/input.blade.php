<div class="form-group">
    <label for="{{$wrapper}}">{{$title}}</label>
    <input class="form-control" type="text" name="{{$wrapper}}[{{$key}}]" value="{{$value}}">
</div>