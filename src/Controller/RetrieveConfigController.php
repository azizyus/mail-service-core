<?php


namespace Azizyus\MailService\Controller;


use Azizyus\MailService\Enums\RequestEnums;
use Azizyus\MailService\Helpers\ConfigFinder;
use Azizyus\MailService\Helpers\RequestParser;
use Illuminate\Http\Request;

class RetrieveConfigController
{

    /**
     * RetrieveConfigController constructor.
     *
     * @param Request $request
     *
     * @throws \Azizyus\MailService\Exceptions\WrongFormatException
     *
     * returns config file as json
     *
     */

    public function retrieve(Request $request)
    {

        $requestParser = new RequestParser();
        $secret = $requestParser->getFromRequestOnlyExist($request,RequestEnums::_secretKey);

        $configFinder = new ConfigFinder();

        return $configFinder->findConfigWithSecretKey($secret);



    }

}