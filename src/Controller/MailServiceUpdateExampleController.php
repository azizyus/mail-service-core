<?php


namespace Azizyus\MailService\Controller;



use Azizyus\MailService\Requests\RetrieveConfigRequest;
use Azizyus\MailService\Requests\UpdateRequest;
use Illuminate\Http\Request;

class MailServiceUpdateExampleController
{


    public function updatePage(Request $request)
    {

        $retrieveConfigRequest = new RetrieveConfigRequest();
        $retrieveConfigResult = $retrieveConfigRequest->retrieve();
        $formBuild = $retrieveConfigResult->buildForm();


        return view("MailService::example-update")->with([

            "form" => $formBuild,
            "postRoute" => route("mailService.config.update.request")

        ]);
    }


    public function update(Request $request)
    {
        $updateRequest = new UpdateRequest();
        $result = $updateRequest->request($request);
        return $result->message;
    }


}