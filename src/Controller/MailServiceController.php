<?php

namespace Azizyus\MailService\Controller;

use App\Http\Controllers\Controller;
use Azizyus\MailService\Factories\ExampleRequest;
use Azizyus\MailService\Helpers\ConfigFinder;
use Azizyus\MailService\Helpers\RequestParser;
use Azizyus\MailService\MailService;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class MailServiceController extends Controller
{

    public function sendMail(Request $request)
    {

//        $request = ExampleRequest::make();

        $mailService = new MailService($request);
        $mailService->sendMail();

        return ["message" => "success","status" => 1];

    }


}