<?php


namespace Azizyus\MailService\Controller;


use Azizyus\MailService\Enums\RequestEnums;
use Azizyus\MailService\Helpers\ConfigRepository;
use Azizyus\MailService\Exceptions\CantFindEmailKey;
use Azizyus\MailService\Factories\ExampleUpdateRequest;
use Azizyus\MailService\Helpers\RequestParser;
use Illuminate\Http\Request;

class CRUDController
{




    public function update(Request $request)
    {

        ## READ PARAMETERS FROM REQUEST ##
        $requestParser = new RequestParser();

        $secretKey = $requestParser->getSecretKey($request);
        $data =  $requestParser->getFromRequestOnlyExist($request,RequestEnums::_dataKey);




        ## READ PARAMETERS FROM REQUEST ##

        ## UPDATE ##
        $configUpdater = new ConfigRepository();
        $result = $configUpdater->updateEmailKeyConfig($secretKey,$data);
        ## UPDATE ##


        if($result)  return ["message" => "success","status" => 1];
        else
        {
            throw new CantFindEmailKey("cant find your email key");
        }

    }






}