<?php

namespace Azizyus\MailService\Controller;

use App\Http\Controllers\Controller;
use Azizyus\MailService\Factories\ExampleRequest;
use Azizyus\MailService\Factories\MailRequestFactory;
use Azizyus\MailService\Helpers\ConfigFinder;
use Azizyus\MailService\Requests\MailRequest;
use Azizyus\MailService\Helpers\RequestParser;
use Azizyus\MailService\MailService;
use Azizyus\MailService\Results\MailRequestResult;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class MailServiceExampleController extends Controller
{


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     *
     * thats where you serve your form, your form posts to exampleGateway
     * and your gateway posts to where ever you installed this thing
     *
     */
    public function examplePage()
    {

        return view("MailService::example");

    }

    public function examplePage2()
    {
        return view("MailService::example-2");
    }

    public function exampleGateway(Request $request)
    {
        $mailRequest = MailRequestFactory::makeViaEnv();
        $result = $mailRequest->request($request);

        if($result->status)
        {
            return "process is successful"; //redirect your user to where ever you want
        }
        else return "there is an error {$result->errorMessage}"; //tell him there is an error or show you error modal etc etc

    }

}