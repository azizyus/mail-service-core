<?php


namespace Azizyus\MailService\Controller\Web;


use App\Http\Controllers\Controller;
use Azizyus\MailService\Enums\ConfigFileEnums;
use Azizyus\MailService\Enums\RequestEnums;
use Azizyus\MailService\Factories\ClientConfigFinderFactory;
use Azizyus\MailService\Helpers\ConfigFileFormBuilder;
use Azizyus\MailService\Helpers\ConfigFinder;
use Azizyus\MailService\Helpers\ConfigRepository;
use Azizyus\MailService\Helpers\JSONDecoder;
use Azizyus\MailService\Helpers\RequestParser;
use Azizyus\MailService\Requests\UpdateRequest;
use Azizyus\MailService\Results\RetrieveConfigResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

class MailServiceUpdateConfigController extends Controller
{

    public $requestParser;
    public $secret;
    public $configFinder;
    public $isSecretExist;
    public $configRepository;
    public $configFormBuilder;
    public function __construct(Request $request)
    {
        $this->requestParser = new RequestParser();
        $this->secret = $this->requestParser->getFromRequestOnlyExist($request,RequestEnums::_secretKey);
        if($this->secret == null)
        {
            \redirect()->route("index")->send()->with([
                "errorMessage" => "Your secret key is null"
            ]);
        }
        $this->configFinder = new ConfigFinder();
        $this->isSecretExist = $this->configFinder->checkSecretKeyExist($this->secret);

        $this->configRepository  = new ConfigRepository();
        $this->configFormBuilder = new ConfigFileFormBuilder();

        if(!$this->isSecretExist)
        {
            \redirect()->route("index")->send()->with([
                "errorMessage" => "Your SecretKey doesnt exist be sure there is no typo or dumb space"
            ]);
        }
    }

    public function edit(Request $request)
    {


        $jsonOutput = $this->configFinder->findConfigWithSecretKey($this->secret);

        $this->configFormBuilder->setDecodedJson(JSONDecoder::decode($jsonOutput));

        $data = [
            "form" => $this->configFormBuilder->buildForm(),
            "secret" => $this->secret
        ];

        return view("edit")->with($data);


    }

    public function update(Request $request)
    {

        ## READ PARAMETERS FROM REQUEST ##
        $data = $request->all();

        ## UPDATE ##
        $this->configRepository->updateEmailKeyConfig($this->secret,$data);
        ## UPDATE ##

        return Redirect::route("index")->with([
            "successMessage"=>"Your config file updated",
        ]);

    }



    public function create(Request $request)
    {

        return view("add")->with([
            "secret"=>$this->secret,
        ]);

    }

    public function store(Request $request)
    {
        ## READ PARAMETERS FROM REQUEST ##
        $data = $request->all();


        $newData[Str::slug($data[RequestEnums::_emailKey])] = [

            ConfigFileEnums::_TARGET => $this->requestParser->getFromRequestOnlyExist($request,ConfigFileEnums::_TARGET),
            ConfigFileEnums::_SUBJECT => $this->requestParser->getFromRequestOnlyExist($request,ConfigFileEnums::_SUBJECT),

        ];

        ## UPDATE ##
        $this->configRepository->updateEmailKeyConfig($this->secret,$newData,true);
        ## UPDATE ##

        return Redirect::route("index")->with([
            "successMessage"=>"Your config file updated",
        ]);

    }


    public function delete(Request $request)
    {

        $secret = $this->secret;
        $emailKey = $this->requestParser->getEmailKey($request);

        $this->configRepository->deleteEmailKey($secret,$emailKey);



        return Redirect::route("index")->with([
            "successMessage"=>"Your [$emailKey] emailKey deleted",
        ]);


    }


}