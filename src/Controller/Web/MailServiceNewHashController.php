<?php


namespace Azizyus\MailService\Controller\Web;


use App\Http\Controllers\Controller;
use Azizyus\MailService\Enums\RequestEnums;
use Azizyus\MailService\Helpers\ConfigRepository;
use Azizyus\MailService\Helpers\MasterHashChecker;
use Illuminate\Http\Request;

class MailServiceNewHashController extends Controller
{

    public $sessionKey  = "_HASH_";
    public function spawnNewFile(Request $request)
    {

        $hash = $request->get(RequestEnums::_hash);
        $masterHashChecker = new MasterHashChecker();
        $isTrue = $masterHashChecker->checkRequestedHash($hash);

        if($isTrue)
        {
            $configRepository = new ConfigRepository();
            $newHash = $configRepository->createNewFile();
            session()->flash($this->sessionKey,$newHash);
            return redirect()->route("config.newFile.flash")->send();
        }
        else return "your master hash is wrong";

    }

    public function flashNewHash()
    {

        $newHash = session()->get($this->sessionKey);
        return view("new-hash")->with([
            "newHash" => $newHash,
        ]);

    }


}