<?php


namespace Azizyus\MailService\Enums;


class RequestEnums
{

    const _secretKey = "secret";
    const _emailKey = "emailKey";
    const _dataKey = "data";
    const _optionNameKey = "optionName";
    const _newOptionValueKey = "newOptionValue";
    const _separatorKey = "separator";
    const _hash = "hash";
}