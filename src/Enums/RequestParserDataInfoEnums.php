<?php


namespace Azizyus\MailService\Enums;


class RequestParserDataInfoEnums
{


    const  _PAIR_KEY = "pair";
    const _LABEL_KEY = "label";
    const _SORT_KEY  = "sort";
    const _GLUE_KEY  = "glue";

}