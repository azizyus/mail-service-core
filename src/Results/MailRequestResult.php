<?php


namespace Azizyus\MailService\Results;


class MailRequestResult
{


    public $status=0;
    public $errorMessage = "no error message received";
    public $debugMessage = "no debug message";
    public $message = "";

    public function setDecoded(array $decoded)
    {
        $this->status = $decoded["status"];
        $this->errorMessage = isset($decoded["error"]) ? $decoded["error"] :"";
        $this->message = isset($decoded["message"]) ? $decoded["message"] : "";
        $this->debugMessage = isset($decoded["debugMessage"]) ? $decoded["debugMessage"] : "";
    }

}