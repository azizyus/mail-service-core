<?php


namespace Azizyus\MailService\Results;


use Azizyus\MailService\Helpers\ConfigFileFormBuilder;
use Azizyus\MailService\Helpers\JSONDecoder;

class RetrieveConfigResult
{
    public $json;
    public function setResult($json)
    {
        $this->json = $json;
    }

    /**
     *
     *
     * emailKey will be label and wrapper array
     *
     */
    public function buildForm()
    {
        $decoded = JSONDecoder::decode($this->json);
        $configFileFormBuilder = new ConfigFileFormBuilder();
        $configFileFormBuilder->setDecodedJson($decoded);
        return $configFileFormBuilder->buildForm();

    }

}