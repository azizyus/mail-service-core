<?php


namespace Azizyus\MailService\Results;


class MailConfiguration
{


    public $mandatoryKeys = [

        "target",
        "subject"

    ];

    public $key = "";


    public $target;
    public $subject;
    public function __construct($key)
    {
        $this->key = $key;
    }

    public function setData(array $data)
    {

        foreach ($this->mandatoryKeys as $key)
        {

            if(!in_array($key,array_keys($data)))
                throw new \Exception("cant found one of mandatory keys [$key]");

        }


        $this->target = $data["target"];
        $this->subject = $data["subject"];
    }



}