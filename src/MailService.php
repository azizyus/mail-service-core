<?php


namespace Azizyus\MailService;


use Azizyus\MailService\Exceptions\CantSendMailException;
use Azizyus\MailService\Helpers\ConfigFinder;
use Azizyus\MailService\Helpers\RequestParser;
use Azizyus\MailService\Notifications\MailNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class MailService
{


    protected $configFinder;
    protected $request;
    protected $requestParser;
    public function __construct(Request $request)
    {
        $this->configFinder = new ConfigFinder();
        $this->setRequest($request);
        $this->requestParser = new RequestParser();
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;
    }


    private function getConfig()
    {
        $secretKey = $this->requestParser->getSecretKey($this->request);
        $emailKey = $this->requestParser->getEmailKey($this->request);

        return $this->configFinder->findMailConfiguration($secretKey,$emailKey);
    }

    public function sendMail()
    {
        $config = $this->getConfig();
        $data = $this->requestParser->getData($this->request);
        try
        {
            Notification::route("mail",[$config->target])->notifyNow(new MailNotification($config,$data));
        }
        catch (\Exception $exception)
        {
            throw new CantSendMailException($exception,"mail cant send, something wrong, be sure installation has right credentials");
        }


    }



}