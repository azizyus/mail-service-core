<?php


namespace Azizyus\MailService\Factories;


use Azizyus\MailService\Helpers\ClientConfigFinder;

class ClientConfigFinderFactory
{

    public static function makeFormEnv()
    {
        $clientConfigFinder = new ClientConfigFinder();
        $clientConfigFinder->setSecret( env("MAIL_SERVICE_SECRET"));
        $clientConfigFinder->setServer(env("MAIL_SERVICE_SERVER"));
        return $clientConfigFinder;
    }

}