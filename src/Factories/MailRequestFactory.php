<?php


namespace Azizyus\MailService\Factories;


use Azizyus\MailService\Helpers\ClientConfigFinder;
use Azizyus\MailService\Requests\MailRequest;

class MailRequestFactory
{

    public static function makeViaEnv()
    {
        $mailRequest = new MailRequest();
        ## FIND CONFIG ##
        $clientConfigFinder = ClientConfigFinderFactory::makeFormEnv();
        ## FIND CONFIG ##
        $mailRequest->setClientConfigFinder($clientConfigFinder);
        return $mailRequest;
    }

}