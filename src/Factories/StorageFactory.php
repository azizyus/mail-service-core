<?php


namespace Azizyus\MailService\Factories;


use Azizyus\MailService\Helpers\ConfigFinder;
use Illuminate\Support\Facades\Storage;

class StorageFactory
{

    public static function make()
    {

        return Storage::disk("local");

    }

}