<?php


namespace Azizyus\MailService\Factories;


use Azizyus\MailService\Enums\RequestEnums;
use Azizyus\MailService\Helpers\ConfigFinder;
use Azizyus\MailService\Helpers\RequestParser;
use Illuminate\Http\Request;

class ExampleRequest
{


    public static function make()
    {

        $request = new Request();


        $request->merge([
            RequestEnums::_emailKey => "contact_email",
            RequestEnums::_secretKey => "4e9e9e9c2c289c651f767640377336c2",
            RequestEnums::_separatorKey => ".",
            "data" => [

                "label:Adult" => 1,
                "label:Child" => 2,
                "label:Cities.glue:," => [

                    "Konstantin",
                    "Atina",
                    "Rome",
                    "Paris",
                    "London"

                ],

                "label:Date.pair:Dates.sort:2" => "11",
                "label:Date.pair:Dates.sort:3" => "10",
                "label:Date.pair:Dates.sort:1" => "2019",



            ]
        ]);


        return $request;


    }

    public static function brokenMake()
    {
        $request = self::make();
        $request->merge(["data"=>["broken"=>"_TEST_DATA_"]]);
        return $request;
    }


}