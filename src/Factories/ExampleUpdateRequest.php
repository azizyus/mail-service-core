<?php


namespace Azizyus\MailService\Factories;


use Azizyus\MailService\Enums\RequestEnums;
use Azizyus\MailService\Helpers\ClientConfigFinder;
use Azizyus\MailService\Helpers\RequestParser;
use Illuminate\Http\Request;

class ExampleUpdateRequest
{

    public static function make()
    {

        $clientConfigFinder = ClientConfigFinderFactory::makeFormEnv();

        $request = new Request();

        $request->merge([
            RequestEnums::_emailKey => "contact_email",
            RequestEnums::_secretKey => $clientConfigFinder->secret,
            RequestEnums::_optionNameKey => "target",
            RequestEnums::_newOptionValueKey => "can123@diffea.com"
        ]);


        return $request;

    }

    public static function makeBroken()
    {
        $request = self::make();
        $request->merge([
            RequestEnums::_optionNameKey => "KEK"
        ]);

        return $request;

    }

}