<?php


namespace Azizyus\MailService\Exceptions;


use Exception;
use Throwable;

class BaseException extends Exception
{

    private $realException;

    public function __construct(Exception $realException,$message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->realException = $realException;
    }

    public function getRealExceptionMessage()
    {
        return $this->realException->getMessage();
    }

    public function getRealExceptionLine()
    {
        return $this->realException->getLine();
    }

    public function getRealExceptionFileName()
    {
        return $this->realException->getFile();
    }

}