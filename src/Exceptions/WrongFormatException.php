<?php

namespace Azizyus\MailService\Exceptions;

use Exception;

class WrongFormatException extends BaseException implements IApiException
{
    //
}
