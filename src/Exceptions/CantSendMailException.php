<?php

namespace Azizyus\MailService\Exceptions;

use Exception;
use Throwable;

class CantSendMailException extends BaseException implements IApiException
{

}
