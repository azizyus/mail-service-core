<?php

namespace Azizyus\MailService\Exceptions;

use Exception;

class CantFindRequestArrayKeyException extends Exception implements IApiException
{
    //
}
