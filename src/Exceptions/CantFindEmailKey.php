<?php

namespace Azizyus\MailService\Exceptions;

use Exception;

class CantFindEmailKey extends Exception implements IApiException
{
    //
}
