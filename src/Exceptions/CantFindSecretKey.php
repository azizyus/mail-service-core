<?php

namespace Azizyus\MailService\Exceptions;

use Exception;

class CantFindSecretKey extends Exception implements IApiException
{
    //
}
