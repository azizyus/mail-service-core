<?php

namespace Azizyus\MailService\Exceptions;

use Exception;

class JsonParseException extends Exception implements IApiException
{
    //
}
