<?php


namespace Azizyus\MailService;


use Azizyus\MailService\Enums\RequestEnums;
use Azizyus\MailService\Exceptions\Handler;
use Carbon\Laravel\ServiceProvider;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

class MailServiceProvider extends ServiceProvider
{

    public $namespace = "Azizyus\\MailService\\Controller\\";

    public function boot()
    {

        $this->loadViewsFrom(__DIR__ . "/Html","MailService");
        $this->mergeConfigFrom(__DIR__ . "/Config/mail_service.php","mail_service");
        $this->app["router"]
        ->any("send-mail","{$this->namespace}MailServiceController@sendMail")->name("mailService.sendMail");


        ## EXAMPLE ##
        $this->app["router"]
            ->get("example","{$this->namespace}MailServiceExampleController@examplePage")->name("mailService.examplePage");

        $this->app["router"]
            ->get("example-2","{$this->namespace}MailServiceExampleController@examplePage2")->name("mailService.examplePage2");


        $this->app["router"]
            ->post("exampleGateway","{$this->namespace}MailServiceExampleController@exampleGateway")->name("mailService.exampleGateway");

        ## EXAMPLE ##

        ## BASIC TEST ##
        $this->app["router"]
            ->get("test","{$this->namespace}MailServiceTestController@test")->name("mailService.test");

        ## BASIC TEST ##


        ## OPTION UPDATE ##


        $this->app["router"] //this one handles request and save it to files
        ->any("update","{$this->namespace}CRUDController@update")->name("mailService.config.update");



        $this->app["router"] //returns specific secret key'd file as json
        ->any("retrieve-config","{$this->namespace}RetrieveConfigController@retrieve")->name("mailService.config.retrieve");



        $this->app["router"] //retrieves config and builds edit form
            ->get("update-config","{$this->namespace}MailServiceUpdateExampleController@updatePage")->name("mailService.config.update.form");

        $this->app["router"] //retrieves config and builds edit form
        ->any("update-config-post","{$this->namespace}MailServiceUpdateExampleController@update")->name("mailService.config.update.request");


        ## OPTION UPDATE ##


        ## WEB ROUTES ##
        ## UPDATE ##
        $this->app["router"]
            ->get("show-config","{$this->namespace}Web\MailServiceUpdateConfigController@edit")->name("config.edit");

        $this->app["router"]
            ->post("update-config","{$this->namespace}Web\MailServiceUpdateConfigController@update")->name("config.update");

        ## UPDATE ##


        ## ADD ##
        $this->app["router"]
            ->get("create-config","{$this->namespace}Web\MailServiceUpdateConfigController@create")->name("config.create");

        $this->app["router"]
            ->post("store-config","{$this->namespace}Web\MailServiceUpdateConfigController@store")->name("config.store");

        ## ADD ##

        ## DELETE ##
        $this->app["router"]
        ->get("delete-email-key","{$this->namespace}Web\MailServiceUpdateConfigController@delete")->name("config.email.delete");
        ## DELETE ##

        ## CREATE NEW CONFIG FILE ##
        $this->app["router"]
        ->get("new-hash","{$this->namespace}Web\MailServiceNewHashController@spawnNewFile")->name("config.newFile");

        $this->app["router"]
            ->get("new-hash-flash","{$this->namespace}Web\MailServiceNewHashController@flashNewHash")->name("config.newFile.flash");
        ## CREATE NEW CONFIG FILE ##

        ## WEB ROUTES ##



//        ## EXCEPTION HANDLER REGISTER ##
        $this->app->singleton(
            ExceptionHandler::class,
            Handler::class
        );
//        ## EXCEPTION HANDLER REGISTER ##

        $path = config("mail_service.directory");

        if(!Storage::exists($path)) {
            Storage::makeDirectory($path);
        }



    }

}