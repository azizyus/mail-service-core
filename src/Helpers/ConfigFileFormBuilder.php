<?php


namespace Azizyus\MailService\Helpers;


use Azizyus\MailService\Enums\RequestEnums;

class ConfigFileFormBuilder
{

    private $decodedJson;

    public function setDecodedJson(array $decodedJson)
    {
        $this->decodedJson = $decodedJson;
    }

    public function buildForm()
    {
        $html = "";
        foreach ($this->decodedJson as $wrapper => $data)
        {

            $form = "";

            foreach ($data as $key => $value)
            {
                $form .= view("MailService::Form.input")->with([
                    "wrapper" => $wrapper,
                    "key" => $key,
                    "value" => $value,
                    "title" => $key
                ])->render();
            }

            $html.= view("MailService::Form.wrapper")->with([
                "form" => $form,
                "header" => $wrapper,
                RequestEnums::_emailKey  => $wrapper
            ])->render();

        }

        return $html;
    }

}