<?php


namespace Azizyus\MailService\Helpers;


class MasterHashChecker
{


    public function getMasterHash()
    {
        return env("MAIL_SERVICE_MASTER_KEY");
    }


    public function checkRequestedHash($requestedHash)
    {

        $masterHash = $this->getMasterHash();

        if($masterHash == $requestedHash)
        {
            return true;
        }


        return false;

    }

}