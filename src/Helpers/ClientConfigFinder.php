<?php


namespace Azizyus\MailService\Helpers;


class ClientConfigFinder
{

    public $secret;
    public $server;

    public $sendMailPath = "/send-mail";
    public $updateOptionPath = "/update";
    public $retrieveConfigPath = "/retrieve-config";

    public function setSecret(String $secret)
    {
        $this->secret = $secret;
    }

    public function setServer(String $server)
    {
        $this->server = $server;
    }



}