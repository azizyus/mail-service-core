<?php


namespace Azizyus\MailService\Helpers;


class ArrayHelper
{


    public static function getIndex($index,$array,$default = "")
    {
        if(isset($array[$index])) return $array[$index];
        return $default;
    }

}