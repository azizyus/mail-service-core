<?php


namespace Azizyus\MailService\Helpers;


use Azizyus\MailService\Exceptions\CantFindEmailKey;
use Azizyus\MailService\Exceptions\CantFindSecretKey;
use Azizyus\MailService\Exceptions\JsonParseException;
use Azizyus\MailService\Factories\StorageFactory;
use Azizyus\MailService\Results\MailConfiguration;
use Illuminate\Support\Facades\Storage;

class ConfigFinder
{



    protected $configurationDirectory = "";
    protected $storage;

    public function __construct()
    {
        $this->storage = StorageFactory::make();
        $this->configurationDirectory = config("mail_service.directory");
    }

    public function makeConfigFilePath(String $secretKey)
    {
        return $this->configurationDirectory."/$secretKey";
    }

    public function findConfigWithSecretKey($secretKey)
    {
        $files = $this->getAll();


        foreach ($files as $file)
        {
            $fileSecretKey = basename($file);

            if($fileSecretKey == $secretKey)
            {
                $content  = $this->storage->get($file);
                return $content;
            }
        }

        throw new CantFindSecretKey("cant find your secret key named file");

    }

    public function checkSecretKeyExist($secretKey)
    {
        return $this->storage->exists($this->makeConfigFilePath($secretKey));
    }

    public function findConfigWithSecretKeyDecodedAsArray($secretKey)
    {
        return JSONDecoder::decode($this->findConfigWithSecretKey($secretKey));
    }

    public function getAll()
    {
        return  $this->storage->files($this->configurationDirectory);
    }


    public function findMailConfiguration($secretKey, $emailKey)
    {

        $content = $this->findConfigWithSecretKey($secretKey);


            $contentArray = json_decode($content,true);

            if($contentArray == null)
                throw new JsonParseException("i cant parse requested json file [$secretKey]");


        foreach ($contentArray as $key => $mailConfigurationData)
        {

           if($key == $emailKey)
           {
               $mailConfiguration = new MailConfiguration($key);
               $mailConfiguration->setData($mailConfigurationData);
               return $mailConfiguration;

           }

        }

        throw new CantFindEmailKey("cant find your email key, be sure its right [$key]");

    }

}