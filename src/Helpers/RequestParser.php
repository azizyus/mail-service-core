<?php


namespace Azizyus\MailService\Helpers;


use Azizyus\MailService\Enums\RequestEnums;
use Azizyus\MailService\Enums\RequestParserDataInfoEnums;
use Azizyus\MailService\Enums\RequestParserTypeEnums;
use Azizyus\MailService\Exceptions\CantFindRequestArrayKeyException;
use Azizyus\MailService\Exceptions\WrongFormatException;
use Illuminate\Http\Request;

class RequestParser
{

    protected $defaultArrayGlue = ", "; //yes there is space its not typo

    public function getSecretKey(Request $request)
    {
        return $this->getFromRequestOnlyExist($request,RequestEnums::_secretKey);
    }

    public function getEmailKey(Request $request)
    {
        return $this->getFromRequestOnlyExist($request,RequestEnums::_emailKey);
    }

    public function getOptionName(Request $request)
    {
        return $this->getFromRequestOnlyExist($request,RequestEnums::_optionNameKey);
    }

    public function getNewOptionValue(Request $request)
    {
        return $this->getFromRequestOnlyExist($request,RequestEnums::_newOptionValueKey);
    }

    public function getFromRequestOnlyExist(Request $request,$key)
    {
        $isExist = $request->exists($key);

        if($isExist)
        {
            return $request->get($key,null);
        }

        throw new CantFindRequestArrayKeyException("[$key] key doesnt exist in your request and its mandatory so provide it");

    }

    public function getData(Request $request)
    {
        $newData = [];

        try
        {
            $key = RequestEnums::_dataKey;
            $postedData = $this->getFromRequestOnlyExist($request,RequestEnums::_dataKey);
            $separator = $request->get(RequestEnums::_separatorKey,",");
            $processedPairs = [];
            foreach ($postedData as $dataInfoString => $data)
            {



                ## RETRIEVE DATA OF SINGLE VALUE ##
                $parsedDataInfo = $this->parseDataInfo($dataInfoString,$separator);
                ## RETRIEVE DATA OF SINGLE VALUE ##

                ## PROCESS VALUE ACCORDING TO TYPE ##
                $type = $this->getRequestDataType($data);
                $label = ArrayHelper::getIndex(RequestParserDataInfoEnums::_LABEL_KEY,$parsedDataInfo,"");
                $glue = ArrayHelper::getIndex(RequestParserDataInfoEnums::_GLUE_KEY,$parsedDataInfo,$this->defaultArrayGlue);

                //check current thing has pair, if its we need check did i process that or not
                if(isset($parsedDataInfo["pair"]))
                {

                    /**
                     *
                     *
                     * EVERY PART OF PAIR WILL BE PROCESS SAME ITERATION
                     *
                     */
                   $pairString = $parsedDataInfo[RequestParserDataInfoEnums::_PAIR_KEY];
                   if(!in_array($pairString,$processedPairs)) //check is already processed
                   {  ## SEARCH OTHER PAIRS ##
                       $value = $this->concreteOtherPairs($postedData,$pairString,$separator);

                       $processedPairs[] = $pairString;

                       ## SEARCH OTHER PAIRS ##
                   }
                   else continue;

                }
                else
                {
                    //if glue is null than the default value will be provided via Request::get(); (which is ",")
                    $value = $this->parseDataValue($data,$type,$glue);
                    ## PROCESS VALUE ACCORDING TO TYPE ##
                }







                $newData[] = [

                    "title" => $label,
                    "value" => $value
                ];

            }


        }
        catch (\Exception $exception)
        {
            throw new WrongFormatException($exception,"your [$key] parameter is probably wrong formatted or broken");
        }
//


        return $newData;

    }


    public function concreteOtherPairs($postedData,$pairString,$separator)
    {

        $newValueAsArray = [];
        $glue = "-";
        foreach ($postedData as $infoString => $data)
        {
            $parsedInfo = $this->parseDataInfo($infoString,$separator);

            $foundPair = ArrayHelper::getIndex(RequestParserDataInfoEnums::_PAIR_KEY,$parsedInfo,null);

            if($pairString == $foundPair)
            {

                $sort = ArrayHelper::getIndex(RequestParserDataInfoEnums::_SORT_KEY,$parsedInfo,0);

                $newValueAsArray[] = [
                    "value" => $data,
                    "sort" => $sort
                ];
            }


        }

        //i dont even fucking know how this language sorts this shit but collect(); will do the thing
        $newValueAsArray = collect($newValueAsArray)->sortBy("sort")->toArray();

        //we dont need sort anymore so it will be removed
        $newValueToString = implode($glue,array_map(function($newValueItem){
            return $newValueItem["value"];
        },$newValueAsArray));

        return $newValueToString;

    }

    public function parseDataInfo(String $dataInfoString,$separator)
    {
        $dataInfos = explode($separator,$dataInfoString);
        $parsedDataInfo = [];
        foreach ($dataInfos as $dataInfo)
        {
            list($dataKey,$dataValue) = explode(":",$dataInfo);
            $parsedDataInfo[$dataKey] = $dataValue;
        }

        return $parsedDataInfo;
    }

    public function parseDataValue($item,$type,$glue=", ") : String
    {

        $text = " - ";

        switch ($type)
        {
            case RequestParserTypeEnums::_STRING:
                $text = $item;
                break;

            case RequestParserTypeEnums::_ARRAY:
                $text =  implode($glue,$item);
                break;


            default:
                break;



        }
        return $text;

    }
    public function getRequestDataType($item)
    {
        if(is_array($item)) return RequestParserTypeEnums::_ARRAY;
        return RequestParserTypeEnums::_STRING;

    }
}