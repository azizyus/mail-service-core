<?php


namespace Azizyus\MailService\Helpers;


use Azizyus\MailService\Exceptions\CantFindEmailKey;
use Azizyus\MailService\Helpers\ConfigFinder;
use Azizyus\MailService\Helpers\JSONDecoder;

class ConfigRepository extends ConfigFinder
{


    /**
     * @param $secretKey
     * @param $emailKey
     * @param $option
     * @param $newValue
     *
     *
     * if there is no secret key this class with throw exception so you cant create files with this
     * method
     *
     * this is basically updates already existed keys YOU CANT INJECT THEM BECAUSE IM CRECKNING by isset();
     * to being sure there will be no key added via this injection
     *
     */
    public function updateEmailKeyConfig($secretKey, $newData,$addNew=false)
    {

        $configuration = $this->findConfigWithSecretKeyDecodedAsArray($secretKey);

        foreach ($newData as $key => $data)
        {
           if(isset( $configuration[$key] ) || $addNew)
           {
               $configuration[$key]  = $data;
           }
        }


        $this->storage->put($this->makeConfigFilePath($secretKey),JSONDecoder::encode($configuration));
        return $configuration;




    }


    public function deleteEmailKey($secret,$emailKey)
    {

        $configuration = $this->findConfigWithSecretKeyDecodedAsArray($secret);
        unset($configuration[$emailKey]);
        $this->storage->put($this->makeConfigFilePath($secret),JSONDecoder::encode($configuration));

    }


    public function createNewFile() : String
    {
        $newFileHash = hash("sha256",uniqid());
        $this->storage->put($this->makeConfigFilePath($newFileHash),"");
        return $newFileHash;
    }

}