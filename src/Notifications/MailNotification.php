<?php

namespace Azizyus\MailService\Notifications;

use Azizyus\MailService\Results\MailConfiguration;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MailNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     *
     * data is more like;
     *
     * [
     * TITLE : VALUE,
     * TITLE : VALUE,
     * TITLE : VALUE
     * ]
     *
     */

    public $config;
    public $data;
    public function __construct(MailConfiguration $config, array $data)
    {
        $this->config = $config;
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mailMessage = new MailMessage();


        $mailMessage->greeting($this->config->subject);
        $mailMessage->subject($this->config->subject);
        $mailMessage->from("notification@diffea.com");

        foreach ($this->data as $data)
        {
            $mailMessage->line($data["title"].": ".$data["value"]);
        }

        return $mailMessage;


    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
