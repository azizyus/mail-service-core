<?php


namespace Azizyus\MailService\Requests;


use Azizyus\MailService\Enums\RequestEnums;
use Azizyus\MailService\Factories\ClientConfigFinderFactory;
use Azizyus\MailService\Helpers\ClientConfigFinder;
use Azizyus\MailService\Results\RetrieveConfigResult;
use GuzzleHttp\Client;

class RetrieveConfigRequest
{

    public $clientConfigFinder;
    public function __construct()
    {
        $this->setClientConfigFinder(ClientConfigFinderFactory::makeFormEnv());
    }

    public function setClientConfigFinder(ClientConfigFinder $clientConfigFinder)
    {
        $this->clientConfigFinder = $clientConfigFinder;
    }

    public function retrieve() : RetrieveConfigResult
    {

         $secret = $this->clientConfigFinder->secret;

         $guzzle = new Client();
         $data = [

             RequestEnums::_secretKey => $secret

         ];

         $result = $guzzle->post($this->clientConfigFinder->server.$this->clientConfigFinder->retrieveConfigPath,[

             "form_params" => $data

         ]);

         $json = $result->getBody()->getContents();
         $retrieveConfigResult = new RetrieveConfigResult();
         $retrieveConfigResult->setResult($json);

         return $retrieveConfigResult;



    }

}