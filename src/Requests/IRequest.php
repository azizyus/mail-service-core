<?php


namespace Azizyus\MailService\Requests;


use Illuminate\Http\Request;

interface IRequest
{

    public function request(Request $request);

}