<?php


namespace Azizyus\MailService\Requests;


use Azizyus\MailService\Enums\RequestEnums;
use Azizyus\MailService\Helpers\ClientConfigFinder;
use Azizyus\MailService\Results\MailRequestResult;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MailRequest implements IRequest
{


    protected $clientConfigFinder;

    public function setClientConfigFinder(ClientConfigFinder $clientConfigFinder)
    {
        $this->clientConfigFinder = $clientConfigFinder;
    }

    public function request(Request $request) : MailRequestResult
    {
        $emailKey = $request->get( RequestEnums::_emailKey,"");

        $data = [

            RequestEnums::_secretKey => $this->clientConfigFinder->secret,
            RequestEnums::_emailKey=> $emailKey,
            RequestEnums::_dataKey  => $request->get( RequestEnums::_dataKey)

        ];

        $guzzle = new Client();
        $postRoute = $this->clientConfigFinder->server.$this->clientConfigFinder->sendMailPath;
        $result = $guzzle->post($postRoute,["form_params"=>$data]);
        $mailRequestResult = new MailRequestResult();

        if($result->getStatusCode() == 200)
        {
            $html = $result->getBody()->getContents();

            $decoded = json_decode($html,true);
            $mailRequestResult->setDecoded($decoded);

        }

        return $mailRequestResult;


    }

}