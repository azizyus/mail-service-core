<?php


namespace Azizyus\MailService\Requests;


use Azizyus\MailService\Enums\RequestEnums;
use Azizyus\MailService\Factories\ClientConfigFinderFactory;
use Azizyus\MailService\Helpers\ClientConfigFinder;
use Azizyus\MailService\Helpers\JSONDecoder;
use Azizyus\MailService\Helpers\RequestParser;
use Azizyus\MailService\Results\UpdateRequestResult;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class UpdateRequest implements IRequest
{
    protected $requestParser;
    protected $clientConfigFinder;
    public function __construct()
    {
        $this->requestParser = new RequestParser();
        $this->clientConfigFinder = ClientConfigFinderFactory::makeFormEnv();
    }

    public function request(Request $request)
    {

        $secretKey = $this->clientConfigFinder->secret;

        $guzzle = new Client();

        $data[RequestEnums::_dataKey] = $request->all();
        $data[RequestEnums::_secretKey] = $secretKey;


        $result = $guzzle->post($this->clientConfigFinder->server.$this->clientConfigFinder->updateOptionPath,[

            "form_params" => $data,

        ]);

        $updateRequestResult = new UpdateRequestResult();
        $html = $result->getBody()->getContents();
        $updateRequestResult->setMessage(JSONDecoder::decode($html));

        return $updateRequestResult;

    }

}